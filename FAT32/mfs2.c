#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Basic type definitions
typedef unsigned int        uint32_t;
typedef unsigned short int  uint16_t;
typedef unsigned char        uint8_t;
typedef signed int           int32_t;
typedef signed short int     int16_t;
typedef signed char           int8_t;

#define TRUE 1
#define FALSE 0
#define MAX_NUM_ARGS 10

#define FAT32_BPB_SIZE 90

/**
*
* Stores all of the information in the bios parameter block of the FAT32 file image.
*
*/
typedef struct FAT32_BPB
{
	uint8_t  jmpBoot[3];
	char     OEMName[8];
	uint16_t BytsPerSec;
	uint8_t  SecPerClus;
	uint16_t RsvdSecCnt;
	uint8_t  NumFATS;
	uint16_t RootEntCnt;
	uint16_t TotSec16;
	uint8_t  Media;
	uint16_t FATz16;
	uint16_t SecPerTrk;
	uint16_t NumHeads;
	uint32_t HiddSec;
	uint32_t TotSec32;

	uint32_t FATz32;
	uint16_t ExtFlags;
	uint16_t FSVer;
	uint32_t RootClus;
	uint16_t FSInfo;
	uint16_t BkBootSec;
	uint8_t  Reserved[12];
	uint8_t  DrvNum;
	uint8_t  Reserved1;
	uint8_t  BootSig;
	uint32_t VolID;
	char     VolLab[11];
	char     FileSysType[8];
} FAT32_BPB;

/*
*
* A structure holding the information for a directory in the FAT32 file image.
*
*/
typedef struct directory
{
	char     dir_name[11];
	uint8_t  dir_attr;
	uint8_t  unused[8];
	uint16_t first_cluster_high;
	uint8_t  unused2[4];
	uint16_t first_cluster_low;
	uint32_t file_size;
} directory;

/**
*
* Useful struct that holds basic parameters that get passed around the program.
*
*/
typedef struct session_params
{
	FILE* image;
	FAT32_BPB BPB;
	uint32_t wd_sector;
} session_params;

/*
* Converts a string from mixed case to upper case.
*
* @param src The source string to convert to upper case.
* @param dst The location to put the result in.
*/
void str_toupper(char* src, char* dst, int size);

/*
* Determines if a specified string is prefixed by another string.
*
* @param src The source string to compare with.
* @param cmp The smaller string that src will be checked against.
* @return Either zero or one, depending on if src starts with cmp.
*/
int str_startswith(char* src, char* cmp, int src_size, int dst_size);

/*
* Determines if a specified string is suffixed by another string.
*
* @param src The source string to compare with.
* @param cmp The smaller string that src will be checked against.
* @return Either zero or one, depending on if src ends with cmp.
*/
int str_endswith(char* src, char* cmp, int src_size, int dst_size);

/*
*
* Trims leading and trailing whitespace from a string.
*
* @param s The string to trim
* @param new_length The destination to put the new, trimmed length of the string.
* @return A pointer to the new start of the string.
*/
char* strtrim(char* s, int* new_length);

/*
*
* Splits a string into fragments based on a delimeter.
*
*
* @param s The string to split
* @param out The array of strings that will be split
* @param delim The character to split the string on
* @param split_count The pointer to store the amount of strings that resulted from the split operation.
* @param max_split The maximum amount of strings that could possibly result from the split operation.
*/
void strsplit(char* s, char** out, char delim, int* split_count, int max_split);

/* Shortcut for strsplit that assumed a delimeter of space. */
void parse_cmd(char* raw_cmd, char** args, int* argc, int max_args);

/* Opens a fat32 image file. */
FILE* open_fat32(FAT32_BPB* BPB, char* image_path);

/* Calculates the length of a fat32 filename. */
int fat32_strlen(char* name);

/* Determines whether the specified directory is an actual subdirectory. */
int is_dir(directory* dir);

/* Determines whether the specified file/directory should be visible. */
int is_visible_dir(directory* dir);

/* Converts from a sector to a logical address. */
uint32_t sector_to_address(uint16_t sector, FAT32_BPB BPB);

/* Finds the next logical block in a file/directory. */
int16_t nextlb(uint32_t sector, session_params* params);

/* Gets the set of files and directories in the current working directory. */
void get_wd_attr(session_params* params, directory wd[16]);

/* Finds a directory based on a local space name. */
void get_dir(session_params* params, directory* out, int* found, char* dir_name);

/* Gets the sector of a directory specified by a name. Internally invokes get_dir. */
int16_t get_dir_sector(session_params* params, char* dir_name);

/* Prints out all visible files and directories in the current working directory. */
void show_dir_contents(session_params* params, uint16_t dir_sec);

/* Helper function to print out message indicating there is no current volume opened. */
void no_image();

// Functions to handle the various commands when they get invoked.
void handle_cmd_open(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_close(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_info(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_stat(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_get(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_read(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_cd(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_ls(char* cmd, char** args, int argc, session_params* params);
void handle_cmd_volume(char* cmd, char** args, int argc, session_params* params);
void handle_cmd(char* cmd, char** args, int argc, session_params* params);

void str_toupper(char* src, char* dst, int size)
{
	int i;
	for(i  = 0; i < size; i++)
	{
		dst[i] = toupper((int) src[i]);
	}
}

int str_startswith(char* src, char* cmp, int src_size, int dst_size)
{
	int i = 0;
	int same = 1;

	if(dst_size > src_size)
		return 0;

	while(same && i < dst_size)
	{
		if(src[i] != cmp[i])
			same = 0;

		i++;
	}

	return same;
}

int str_endswith(char* src, char* cmp, int src_size, int dst_size)
{
	int same = 1;
	
	if(dst_size > src_size)
		return 0;

	int i = src_size - 1;
	int j = dst_size - 1;
	

	while(same && j >= 0)
	{
		if(src[i] != cmp[j])
			same = 0;
		
		i--;
		j--;
	}

	return same;
}

char* strtrim(char* s, int* new_length)
{
	int current_length = strlen(s);
	int start = 0;
	int end   = current_length - 1;

	while(isspace(s[start]) && start < current_length)
	{
		start++;
		current_length--;
	}

	while(isspace(s[end]) && end > start)
	{
		end--;
		current_length--;
	}

	s[end + 1] = '\0';

	if(new_length != NULL)
	{
		*new_length = current_length;
	}

	return s + start;
}

void strsplit(char* s, char** out, char delim, int* split_count, int max_split)
{
	int arg = 0;
	char* token = strtok(s, &delim);
	out[0] = token;

	while(arg < max_split && token != NULL)
	{
		arg++;

		token = strtok(NULL, &delim);

		if(token != NULL)
			out[arg] = token;

	}

	if(split_count != NULL)
	{
		*split_count = arg;
	}
}

FILE* open_fat32(FAT32_BPB* BPB, char* image_path)
{
	FILE* file = fopen(image_path, "rb");

	if(file)
	{
		// Read in the bios parameter block
		fseek(file, 0, SEEK_SET);
		fread(&BPB->jmpBoot, sizeof(BPB->jmpBoot), 1, file);
		fread(&BPB->OEMName, sizeof(BPB->OEMName), 1, file);
		fread(&BPB->BytsPerSec, sizeof(BPB->BytsPerSec), 1, file);
		fread(&BPB->SecPerClus, sizeof(BPB->SecPerClus), 1, file);
		fread(&BPB->RsvdSecCnt, sizeof(BPB->RsvdSecCnt), 1, file);
		fread(&BPB->NumFATS, sizeof(BPB->NumFATS), 1, file);
		fread(&BPB->RootEntCnt, sizeof(BPB->RootEntCnt), 1, file);
		fread(&BPB->TotSec16, sizeof(BPB->TotSec16), 1, file);
		fread(&BPB->Media, sizeof(BPB->Media), 1, file);
		fread(&BPB->FATz16, sizeof(BPB->FATz16), 1, file);
		fread(&BPB->SecPerTrk, sizeof(BPB->SecPerTrk), 1, file);
		fread(&BPB->NumHeads, sizeof(BPB->NumHeads), 1, file);
		fread(&BPB->HiddSec, sizeof(BPB->HiddSec), 1, file);
		fread(&BPB->TotSec32, sizeof(BPB->TotSec32), 1, file);
		fread(&BPB->FATz32, sizeof(BPB->FATz32), 1, file);
		fread(&BPB->ExtFlags, sizeof(BPB->ExtFlags), 1, file);
		fread(&BPB->FSVer, sizeof(BPB->FSVer), 1, file);
		fread(&BPB->RootClus, sizeof(BPB->RootClus), 1, file);
		fread(&BPB->FSInfo, sizeof(BPB->FSInfo), 1, file);
		fread(&BPB->BkBootSec, sizeof(BPB->BkBootSec), 1, file);
		fread(&BPB->Reserved, sizeof(BPB->Reserved), 1, file);
		fread(&BPB->DrvNum, sizeof(BPB->DrvNum), 1, file);
		fread(&BPB->Reserved1, sizeof(BPB->Reserved1), 1, file);
		fread(&BPB->BootSig, sizeof(BPB->BootSig), 1, file);
		fread(&BPB->VolID, sizeof(BPB->VolID), 1, file);
		fread(&BPB->VolLab, sizeof(BPB->VolLab), 1, file);
		fread(&BPB->FileSysType, sizeof(BPB->FileSysType), 1, file);
	}

	return file;
}

int fat32_strlen(char* name)
{
	int length = 0;

	while((length < 8 && isalnum(name[length])) || name[length] == '.')
	{
		length++;
	}

	return length;

}

int is_dir(directory* dir)
{
	return dir->dir_attr == 0x10;
}

int is_visible_dir(directory* dir)
{
	return (dir->dir_attr == 0x01 || dir->dir_attr == 0x10 || dir->dir_attr == 0x20) && (dir->dir_name[0] > 0);
}

uint32_t sector_to_address(uint16_t sector, FAT32_BPB BPB)
{
	return BPB.BytsPerSec * ((sector - 2) + BPB.RsvdSecCnt + BPB.NumFATS * BPB.FATz32);
}

int16_t nextlb(uint32_t sector, session_params* params)
{
	uint32_t fat_addr = (params->BPB.BytsPerSec * params->BPB.RsvdSecCnt) + (sector * 4);
	int16_t val;
	fseek(params->image, fat_addr, SEEK_SET);
	fread(&val, 2, 1, params->image);

	return val;
}

void get_wd_attr(session_params* params, directory wd[16])
{
	int i;

	fseek(params->image, sector_to_address(params->wd_sector, params->BPB), SEEK_SET);

	for(i = 0; i < 16; i++)
	{
		fread(&wd[i].dir_name, sizeof(wd[i].dir_name), 1, params->image);
		fread(&wd[i].dir_attr, sizeof(wd[i].dir_attr), 1, params->image);
		fread(&wd[i].unused, sizeof(wd[i].unused), 1, params->image);
		fread(&wd[i].first_cluster_high, sizeof(wd[i].first_cluster_high), 1, params->image);
		fread(&wd[i].unused2, sizeof(wd[i].unused2), 1, params->image);
		fread(&wd[i].first_cluster_low, sizeof(wd[i].first_cluster_low), 1, params->image);
		fread(&wd[i].file_size, sizeof(wd[i].file_size), 1, params->image);
	}

}

void get_dir(session_params* params, directory* out, int* found, char* dir_name)
{
	directory wd[16];
	int i;
	char dir_toupper[12];
	char* dir_split[2];
	int split_size = 1;
	int name_size;

	get_wd_attr(params, wd);

	// Convert entire file/directory name to upper case to ignore casing.
	str_toupper(dir_name, dir_toupper, 12);

	if(strcmp(dir_toupper, "..") == 0)
	{
		dir_split[0] = "..";
	}
	else if(strcmp(dir_toupper, ".") == 0)
	{
		dir_split[0] = ".";
	}
	else
	{
		// Split the string into its file name and file extension.
		strsplit(dir_toupper, dir_split, '.', &split_size, 2);
	}

	name_size = strlen(dir_split[0]);

	*found = 0;

	for(i = 0; i < 16; i++)
	{
		// Only check visible directories and files
		if(is_visible_dir(wd + i))
		{

			// Check if we're a directory or file
			int dir = is_dir(wd + i);

			int fat32_name_length = fat32_strlen(wd[i].dir_name);

			if(fat32_name_length == name_size && strncmp(dir_split[0], wd[i].dir_name, fat32_name_length) == 0)
			{
				if(dir || (split_size > 1 && str_endswith(wd[i].dir_name, dir_split[1], 11, 3)))
				{
					*out = wd[i];
					*found = 1;
				}
			}
		}
	}
}

int16_t get_dir_sector(session_params* params, char* dir_name)
{
	int found;
	directory dir;
	get_dir(params, &dir, &found, dir_name);

	if(!found)
	{
		return -1;
	}
	else
	{
		return dir.first_cluster_low;
	}
}


void show_dir_contents(session_params* params, uint16_t dir_sec)
{
	directory wd[16];
	int i;

	uint16_t wd_sec_prev = params->wd_sector;
	
	params->wd_sector = dir_sec;
	get_wd_attr(params, wd);
	params->wd_sector = wd_sec_prev;


	for(i = 0; i < 16; i++)
	{
		// Make sure the folder is visible
		if(is_visible_dir(wd + i))
		{
			printf("%.11s\n", wd[i].dir_name);
		}
	}
}


void parse_cmd(char* raw_cmd, char** args, int* argc, int max_args)
{
	strsplit(raw_cmd, args, ' ', argc, max_args);
}

void no_image()
{
	printf("Error: file system not open.\n");
}

void handle_cmd_open(char* cmd, char** args, int argc, session_params* params)
{

	params->image = open_fat32(&params->BPB, args[0]);

	if(params->image)
	{
		printf("Successfully opened %s\n", args[0]);
	}
	else
	{
		printf("Failed to open %s\n", args[0]);
	}
}

void handle_cmd_close(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			printf("Error: file system not open.\n");
		}
		else
		{
			fclose(params->image);
			params->image = NULL;
			printf("Successfully closed the fat32 image.\n");
		}
}

void handle_cmd_info(char* cmd, char** args, int argc, session_params* params)
{
	if(!params->image)
	{
		no_image();
		return;
	}
		printf("Field           Hex        Decimal\n");
		printf("BPB_BytesPerSec 0x%08x %010d\n", params->BPB.BytsPerSec, params->BPB.BytsPerSec);
		printf("BPB_SecPerClus  0x%08x %010d\n", params->BPB.SecPerClus, params->BPB.SecPerClus);
		printf("BPB_RsvdSecCnt  0x%08x %010d\n", params->BPB.RsvdSecCnt, params->BPB.RsvdSecCnt);
		printf("BPB_NumFATS     0x%08x %010d\n", params->BPB.NumFATS,    params->BPB.NumFATS);
		printf("BPB_FATz32      0x%08x %010d\n", params->BPB.FATz32,     params->BPB.FATz32);
}

void handle_cmd_stat(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			no_image();
			return;
		}

		int found;
		directory dir;
		get_dir(params, &dir, &found, args[0]);

		if(found)
		{
			printf("Attribute        Size        Starting Cluster Number\n");
			printf("%-17d%-12d%-17d\n", dir.dir_attr, dir.file_size, dir.first_cluster_low);
		}
		else
		{
			printf("Error: file not found\n");
		}
}

void handle_cmd_get(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			no_image();
			return;
		}

		directory dir;
		int found;
		get_dir(params, &dir, &found, args[0]);

		if(found)
		{
			int16_t original_sec = dir.first_cluster_low;
			int16_t dir_sec = original_sec;
			uint8_t read_byte;
			int file_size = dir.file_size;
			int byte = 0;
			int i = 0;
			int sec_byte = 0;

			FILE* out = fopen(args[0], "w");

			while(dir_sec != -1)
			{
				fseek(params->image, sector_to_address(dir_sec, params->BPB), SEEK_SET);

				sec_byte = 0;

				while(sec_byte < params->BPB.BytsPerSec && byte < file_size)
				{

					// Copy the byte over to the other file output stream.


					// Read the byte
					fread(&read_byte, 1, 1, params->image);

					// Write the byte out to the local output file
					fwrite(&read_byte, 1, 1, out);

					sec_byte++;
					byte++;

				}

				dir_sec = nextlb(original_sec + i, params);
				i++;
			}

			fclose(out);
		}
		else
		{
			printf("Error: File not found\n");
		}
}

void handle_cmd_read(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			no_image();
			return;
		}

		int16_t original_sec = get_dir_sector(params, args[0]);
		int16_t dir_sec = original_sec;
		int i = 0;
		int sec_byte;
		int byte = 0;
		int start_byte;
		int amount_bytes;
		uint8_t read_byte;

		start_byte = atoi(args[1]);
		amount_bytes = atoi(args[2]);

		// Seek the first sector
		if(original_sec >= 0)
		{
			while(dir_sec != -1)
			{
				fseek(params->image, sector_to_address(dir_sec, params->BPB), SEEK_SET);

				for(sec_byte = 0; sec_byte < params->BPB.BytsPerSec; sec_byte++, byte++)
				{
					if(byte >= start_byte && byte < start_byte + amount_bytes)
					{
						// Copy the byte out to the standard output stream.
						fread(&read_byte, 1, 1, params->image);
						printf("%d ", read_byte);
					}
				}

				dir_sec = nextlb(original_sec + i, params);
				i++;
			}
			printf("\n");
		}
		else
		{
			printf("Error: File not found\n");
		}
}

void handle_cmd_cd(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			no_image();
			return;
		}

		int max_separators = 10;
		int separators;
		int separator = 0;
		char* paths[max_separators];
		int16_t dir_sector = 0;

		strsplit(args[0], (char**) paths, '/', &separators, max_separators);

		while (separator < separators && dir_sector >= 0)
		{
			// Convert the path to upper case.

			dir_sector = get_dir_sector(params, paths[separator]);

			if(dir_sector >= 0)
			{
				if(dir_sector == 0)
					dir_sector = 2;
				params->wd_sector = dir_sector;
			}

			// Attempt to find directory
			separator++;
		}

		if(dir_sector < 0)
		{
			printf("Path not found.\n");
		}

}

void handle_cmd_ls(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			no_image();
			return;
		}

		if(argc == 1)
		{
				if(strcmp(args[0], ".") == 0)
				{
					show_dir_contents(params, params->wd_sector);
				}
				else if(strcmp(args[0], "..") == 0)
				{
					int16_t parent_dir_sec = get_dir_sector(params, "..");
					
					if(parent_dir_sec >= 0)
					{
											
						if(parent_dir_sec == 0)
							parent_dir_sec = 2;
					
					printf("%d\n", parent_dir_sec);
						show_dir_contents(params, parent_dir_sec);
					}
					else
					{
						printf("This directory has no parent directory.\n");
					}
				}
				else
				{
					printf("Unrecognized first argument %s", args[0]);
				}
		}
		else if(argc == 0)
		{
			show_dir_contents(params, params->wd_sector);
		}
}

void handle_cmd_volume(char* cmd, char** args, int argc, session_params* params)
{
		if(!params->image)
		{
			no_image();
			return;
		}

		printf("Volume name: '%.11s'\n", params->BPB.VolLab);
}

void handle_cmd(char* cmd, char** args, int argc, session_params* params)
{

	// Determine which command handler needs to be executed.

	if(strcmp(cmd, "open") == 0)
	{
		if(argc != 1)
		{
			printf("Incorrect args, correct usage: open <filename>\n");
		}
		else
		{
			handle_cmd_open(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "close") == 0)
	{
		if(argc != 0)
		{
			printf("Incorrect args, correct usage: close\n");
		}
		else
		{
			handle_cmd_close(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "info") == 0)
	{
		if(argc != 0)
		{
			printf("Incorrect args, correct usage: info\n");
		}
		else
		{
			handle_cmd_info(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "stat") == 0)
	{
		if(argc != 1)
		{
			printf("Incorrect args, correct usage: stat <filename> or stat <directory name>\n");
		}
		else
		{
			handle_cmd_stat(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "get") == 0)
	{
		if(argc != 1)
		{
			printf("Incorrect args, correct usage: get <filename>\n");
		}
		else
		{
			handle_cmd_get(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "cd") == 0)
	{
		if(argc != 1)
		{
			printf("Incorrect args, correct usage: cd <directory>\n");
		}
		else
		{
			handle_cmd_cd(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "ls") == 0)
	{
		if(argc > 1)
		{
			printf("Incorrect args, correct usage: ls or ls . or ls ..\n");
		}
		else
		{
			handle_cmd_ls(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "read") == 0)
	{
		if(argc != 3)
		{
			printf("Incorrect args, correct usage: read <filename> <position> <number of bytes>\n");
		}
		else
		{
			handle_cmd_read(cmd, args, argc, params);
		}
	}

	if(strcmp(cmd, "volume") == 0)
	{
		if(argc != 0)
		{
			printf("Incorrect args, correct usage: volume\n");
		}
		else
		{
			handle_cmd_volume(cmd, args, argc, params);
		}
	}
}


int main(int argc, char* argv[])
{
	char line[100];
	char* args[MAX_NUM_ARGS];
	char* cmd;
	int arg_count;
	int i;
	session_params params;

	// Default image is not initialized
	params.image  = NULL;
	// The initial sector is always 2 for the root directory.
	params.wd_sector = 2;

	while(TRUE)
	{

		for(i = 0; i < MAX_NUM_ARGS; i++)
			args[i] = NULL;

		printf("mfs> ");
		fgets(line, 100, stdin);
		parse_cmd(strtrim(line, NULL), args, &arg_count, MAX_NUM_ARGS);

		if(arg_count > 0)
		{
			cmd = args[0];

			handle_cmd(cmd, args + 1, arg_count - 1, &params);
		}
	}
}
