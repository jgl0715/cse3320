/*
*
* Name: Joseph Leavell
* ID: 1001370715
*
*/


#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <semaphore.h>

/*** Constants that define parameters of the simulation ***/

#define MAX_SEATS 3        /* Number of seats in the professor's office */
#define professor_LIMIT 10 /* Number of students the professor can help before he needs a break */
#define MAX_STUDENTS 1000  /* Maximum number of students in the simulation */

#define CLASSA 0
#define CLASSB 1

/* Synchronization variables */
pthread_mutex_t asking_class_lock;
pthread_mutex_t classa_lock;
pthread_mutex_t classb_lock;
pthread_mutex_t break_lock;
sem_t class_semaphore;

/* Basic information about simulation.  They are printed/checked at the end
 * and in assert statements during execution.
 *
 * You are responsible for maintaining the integrity of these variables in the
 * code that you develop.
 */

static int students_in_office;   /* Total numbers of students currently in the office */
static int classa_inoffice;      /* Total numbers of students from class A currently in the office */
static int classb_inoffice;      /* Total numbers of students from class B in the office */
static int classa_consecutive;   /* Amount of students from class a since one student from class b */
static int classb_consecutive;   /* Amount of students from class b since one student from class a */
static int classa_queued;        /* Amount of students from class a waiting at the door */
static int classb_queued;        /* Amount of students from class b waiting at the door */
static int students_since_break; /* Amount of students since professor took last break */
static int asking_class;         /* The class that is currently asking questions */
static int on_break;             /* Whether the professor is on break. A boolean value. */


typedef struct
{
  int arrival_time;  // time between the arrival of this student and the previous student
  int question_time; // time the student needs to spend with the professor
  int student_id;
} student_info;

/* Called at beginning of simulation.
 * TODO: Create/initialize all synchronization
 * variables and other global variables that you add.
 */
static int initialize(student_info *si, char *filename)
{
  students_in_office = 0;
  classa_inoffice = 0;
  classb_inoffice = 0;
  classa_consecutive = 0;
  classb_consecutive = 0;
  classa_queued = 0;
  classb_queued = 0;
  asking_class = -1;
  on_break = 0;
  students_since_break = 0;

  /* Initialize your synchronization variables (and
   * other variables you might use) here
   */
  sem_init(&class_semaphore, 0, 3);

  /* Read in the data file and initialize the student array */
  FILE *fp;

  if((fp=fopen(filename, "r")) == NULL)
  {
    printf("Cannot open input file %s for reading.\n", filename);
    exit(1);
  }

  int i = 0;
  while ( (fscanf(fp, "%d%d\n", &(si[i].arrival_time), &(si[i].question_time))!=EOF) && i < MAX_STUDENTS )
  {
    i++;
  }

 fclose(fp);
 return i;
}

/* Code executed by professor to simulate taking a break
 * You do not need to add anything here.
 */
static void take_break()
{
  printf("The professor is taking a break now.\n");
  sleep(5);
  assert( students_in_office == 0 );
  students_since_break = 0;
}

/* Code for the professor thread. This is fully implemented except for synchronization
 * with the students.  See the comments within the function for details.
 */
void *professorthread(void *junk)
{
  printf("The professor arrived and is starting his office hours\n");

  /* Loop while waiting for students to arrive. */
  while (1)
  {

    if(students_since_break >= professor_LIMIT)
    {
      pthread_mutex_lock(&break_lock);
      on_break = 1;

      printf("professor will take break after current set leaves\n");

      // Spin until all the current students have left
      while(1)
      {
        if(students_in_office == 0)
        {
          break;
        }
      }

      students_since_break = 0;
      take_break();
      pthread_mutex_unlock(&break_lock);
      printf("professor is off break\n");

      on_break = 0;
    }

  }
  pthread_exit(NULL);
}

/*
*
* Puts a student in a class on the queue to go into the office.
*
* @param this_queued A pointer to the amount of students from the current class that are queued to go into the office
* @param this_inoffice A pointer to the integer that represents how many of the current class of students are in office
* @param this_consecutive A pointer to the amount of students from the current class that have gone since a student from the other class has gone
* @param other_consecutive A pointer to the amount of students from the other class that have gone since a student from the current class has gone
*/
void queue_student(int* this_queued, int* this_inoffice, int* this_consecutive, int* other_consecutive)
{

  // We need to increment the queue counter to students from the other classes won't pile up here and create a race condition.
  // They should remain in a busy loop.
  *this_queued += 1;

  // Wait here. The student will be let in once there are < MAX_STUDENTS in the office.
  sem_wait(&class_semaphore);

  // If the professor has already claimed a break, then this student will remain queued until the professor's break is complete.
  pthread_mutex_lock(&break_lock);
  students_in_office += 1;
  students_since_break += 1;
  *this_inoffice += 1;
  *this_consecutive += 1;
  *other_consecutive = 0;
  pthread_mutex_unlock(&break_lock);

  *this_queued -= 1;
}

/*
*
* Helper method for handling when a student wants to enter the professor's office from either class. This function may be called from both classa_enter and classb_enter.
* Assumes that there are only two classes of students, A and B.
*
* @param this_class The value of the current class
* @param other_class The value of the other value
* @param id The id of the current student
* @param this_inoffice A pointer to the integer that represents how many of the current class of students are in office
* @param other_inoffice A pointer to the integer that represents how many of the other class of students are in office
* @param this_queued A pointer to the amount of students from the current class that are queued to go into the office
* @param other_queued A pointer to the amount of students from the other class that are queued to go into the office
* @param this_consecutive A pointer to the amount of students from the current class that have gone since a student from the other class has gone
* @param other_consecutive A pointer to the amount of students from the other class that have gone since a student from the current class has gone
*/
void class_enter(int this_class, int other_class, int id, int* this_inoffice, int* other_inoffice, int* this_queued, int* other_queued, int* this_consecutive, int* other_consecutive)
{

  /*
  *
  * This section of code functions as a block for students that want to get in line while the professor is taking a break.
  * Essentially, these students will pile up until the professor is off of their break.
  *
  */
  pthread_mutex_lock(&break_lock);
  pthread_mutex_unlock(&break_lock);

  /*
  *
  * This section of code checks whether it's time to yield the Q and A period to the other class.
  *
  */
  if(asking_class == this_class && *this_consecutive >= 5)
  {
    // Force an asking class switch at 5 consequtive students
    printf("Switching classes from %d to %d\n", this_class, other_class);
    while(1)
    {
      if(*other_inoffice > 0)
        break;
    }
  }

  if(asking_class != this_class)
  {
    pthread_mutex_lock(&asking_class_lock);
    {

      if(asking_class == other_class)
      {

        // Do a busy loop until there are no students left remaining from the other class.
        // If the professor is on break or students from the other class are still queued
        // to come in, continue looping.
        while(1)
        {
          if(*other_inoffice == 0 && on_break != 1 && *other_queued <= 0)
          {
            break;
          }
        }

        asking_class = this_class;
      }
      else
      {
        asking_class = this_class;
      }

      queue_student(this_queued, this_inoffice, this_consecutive, other_consecutive);

    }
    pthread_mutex_unlock(&asking_class_lock);
  }
  else
  {
    queue_student(this_queued, this_inoffice, this_consecutive, other_consecutive);
  }

}


/* Code executed by a class A student to enter the office.
 * You have to implement this.  Do not delete the assert() statements,
 * but feel free to add your own.
 */
void classa_enter(int id)
{
  class_enter(CLASSA, CLASSB, id, &classa_inoffice, &classb_inoffice, &classa_queued, &classb_queued, &classa_consecutive, &classb_consecutive);
}


/* Code executed by a class B student to enter the office.
 * You have to implement this.  Do not delete the assert() statements,
 * but feel free to add your own.
 */
void classb_enter(int id)
{
  class_enter(CLASSB, CLASSA,id, &classb_inoffice, &classa_inoffice, &classb_queued, &classa_queued, &classa_consecutive, &classb_consecutive);
}

/* Code executed by a student to simulate the time he spends in the office asking questions
 * You do not need to add anything here.
 */
static void ask_questions(int t)
{
  sleep(t);
}


/* Code executed by a class A student when leaving the office.
 * You need to implement this.  Do not delete the assert() statements,
 * but feel free to add as many of your own as you like.
 */
static void classa_leave()
{

  students_in_office -= 1;
  classa_inoffice -= 1;

  // We can release the resource now since the student has left
  sem_post(&class_semaphore);
}

/* Code executed by a class B student when leaving the office.
 * You need to implement this.  Do not delete the assert() statements,
 * but feel free to add as many of your own as you like.
 */
static void classb_leave()
{

  students_in_office -= 1;
  classb_inoffice -= 1;

  // We can release the resource now since the student has left
  sem_post(&class_semaphore);
}

/* Main code for class A student threads.
 * You do not need to change anything here, but you can add
 * debug statements to help you during development/debugging.
 */
void* classa_student(void *si)
{
  student_info *s_info = (student_info*)si;

  printf("Student %d from class A has arrived\n", s_info->student_id);

  /* enter office */
  classa_enter(s_info->student_id);

  printf("Student %d from class A enters the office\n", s_info->student_id);

  assert(students_in_office <= MAX_SEATS && students_in_office >= 0);
  assert(classa_inoffice >= 0 && classa_inoffice <= MAX_SEATS);
  assert(classb_inoffice >= 0 && classb_inoffice <= MAX_SEATS);
  assert(classb_inoffice == 0 );

  /* ask questions  --- do not make changes to the 3 lines below*/
  printf("Student %d from class A starts asking questions for %d minutes\n", s_info->student_id, s_info->question_time);
  ask_questions(s_info->question_time);
  printf("Student %d from class A finishes asking questions and prepares to leave\n", s_info->student_id);

  /* leave office */
  classa_leave();

  printf("Student %d from class A leaves the office\n", s_info->student_id);

  assert(students_in_office <= MAX_SEATS && students_in_office >= 0);
  assert(classb_inoffice >= 0 && classb_inoffice <= MAX_SEATS);
  assert(classa_inoffice >= 0 && classa_inoffice <= MAX_SEATS);

  pthread_exit(NULL);
}

/* Main code for class B student threads.
 * You do not need to change anything here, but you can add
 * debug statements to help you during development/debugging.
 */
void* classb_student(void *si)
{
  student_info *s_info = (student_info*)si;

  printf("Student %d from class B has arrived\n", s_info->student_id);

  /* enter office */
  classb_enter(s_info->student_id);

  printf("Student %d from class B enters the office AT=%d\n", s_info->student_id, s_info->arrival_time);

  assert(students_in_office <= MAX_SEATS && students_in_office >= 0);
  assert(classb_inoffice >= 0 && classb_inoffice <= MAX_SEATS);
  assert(classa_inoffice >= 0 && classa_inoffice <= MAX_SEATS);
  assert(classa_inoffice == 0 );

  printf("Student %d from class B starts asking questions for %d minutes\n", s_info->student_id, s_info->question_time);
  ask_questions(s_info->question_time);
  printf("Student %d from class B finishes asking questions and prepares to leave\n", s_info->student_id);

  /* leave office */
  classb_leave();

  printf("Student %d from class B leaves the office\n", s_info->student_id);

  assert(students_in_office <= MAX_SEATS && students_in_office >= 0);
  assert(classb_inoffice >= 0 && classb_inoffice <= MAX_SEATS);
  assert(classa_inoffice >= 0 && classa_inoffice <= MAX_SEATS);

  pthread_exit(NULL);
}

/* Main function sets up simulation and prints report
 * at the end.
 */
int main(int nargs, char **args)
{
  int i;
  int result;
  int student_type;
  int num_students;
  void *status;
  pthread_t professor_tid;
  pthread_t student_tid[MAX_STUDENTS];
  student_info s_info[MAX_STUDENTS];

  if (nargs != 2)
  {
    printf("Usage: officehour <name of inputfile>\n");
    return EINVAL;
  }

  num_students = initialize(s_info, args[1]);
  if (num_students > MAX_STUDENTS || num_students <= 0)
  {
    printf("Error:  Bad number of student threads. "
           "Maybe there was a problem with your input file?\n");
    return 1;
  }

  printf("Starting officehour simulation with %d students ...\n",
    num_students);

  result = pthread_create(&professor_tid, NULL, professorthread, NULL);

  if (result)
  {
    printf("officehour:  pthread_create failed for professor: %s\n", strerror(result));
    exit(1);
  }

  for (i=0; i < num_students; i++)
  {

    s_info[i].student_id = i;
    sleep(s_info[i].arrival_time);

    student_type = random() % 2;

    if (student_type == CLASSA)
    {
      result = pthread_create(&student_tid[i], NULL, classa_student, (void *)&s_info[i]);
    }
    else // student_type == CLASSB
    {
      result = pthread_create(&student_tid[i], NULL, classb_student, (void *)&s_info[i]);
    }

    if (result)
    {
      printf("officehour: thread_fork failed for student %d: %s\n",
            i, strerror(result));
      exit(1);
    }
  }

  /* wait for all student threads to finish */
  for (i = 0; i < num_students; i++)
  {
    pthread_join(student_tid[i], &status);
  }

  /* tell the professor to finish. */
  pthread_cancel(professor_tid);

  printf("Office hour simulation done.\n");

  return 0;
}
