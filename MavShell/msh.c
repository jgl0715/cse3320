/*
*
* Name: Joseph Leavell
* ID: 1001370715
*
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>

// Locations to look for procedures to run
#define BIN "/bin/"
#define LOC "./"
#define USR_LCL_BIN "/usr/local/bin/"
#define USR_BIN "/usr/bin/"

// Various maximum sizes
#define WHITESPACE " \t\n"
#define MAX_COMMAND_SIZE 255
#define MAX_PATH_SIZE 100
#define MAX_NUM_ARGUMENTS 10
#define HISTORY_SIZE 15

// Command definitions
#define QUIT_CMD "quit"
#define EXIT_CMD "exit"
#define BG_CMD "bg"
#define CD_CMD "cd"
#define LISTPIDS_CMD "listpids"
#define HISTORY_CMD "history"

struct sigaction act_suspend;
struct sigaction act_interrupt;

/*
*
* Stores data about previous commands that have been entered.
*
*/
struct previous_command
{
  char cmd [MAX_COMMAND_SIZE];
  char* args [MAX_NUM_ARGUMENTS];
  int arg_count;
};

// Contains the history of all processes that have been spawned
pid_t pid_history [HISTORY_SIZE];
// Contains the history of all commands that have been entered.
struct previous_command command_history [HISTORY_SIZE];

// The current indices in the array of pid_history and cmd_history.
// Note these values can exceed the length of the array, and that is
// handled in the code.
int pid_history_index;
int cmd_history_index;

/*
* Retreieves the last pid.
*
* @return pid_t The ID of the last process that was spawned by this shell.
*/
pid_t get_last_pid()
{
  if(pid_history_index >= HISTORY_SIZE)
  {
    return pid_history[HISTORY_SIZE - 1];
  }
  else
  {
    return pid_history[pid_history_index];
  }
}

/*
* Handler for suspend signals. Hands the signal off to the last spawned process.
*
* @param signum The number of the signal.
* @param info A pointer to the struct containing basic information about the signal.
* @param ptr
*/
void on_signal_suspend(int signum, siginfo_t* info, void* ptr)
{

  // Last pid will only be greater than zero if the user has already spawned a process
  if(get_last_pid() > 0)
  {
    kill(get_last_pid(), SIGTSTP);
  }
}

/*
* Handler for interrupt signals. Hands the signal off to the last spawned process.
*
* @param signum The number of the signal.
* @param info A pointer to the struct containing basic information about the signal.
* @param ptr
*/
void on_signal_interrupt(int signum, siginfo_t* info, void* ptr)
{

  // Last pid will only be greater than zero if the user has already spawned a process
  if(get_last_pid() > 0)
  {
    kill(get_last_pid(), SIGINT);
  }
}

/*
* Processes when the user wants to quit. For now, simply returns zero to tell the command handler to stop execution.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_quit_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{
  return 0;
}


/*
* Processes when the user wants to background a recently suspended process.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_bg_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{

  pid_t last_pid = get_last_pid();

  // Last pid will only be greater than zero if the user has already spawned a process
  if(last_pid > 0)
  {
    kill(last_pid, SIGCONT);
  }

  return 1;
}


/*
* Processes when the user wants to change the working directory.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_cd_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{

  // Simple
  chdir(args[0]);

  return 1;
}

/*
* Processes when the user wants to list all recently spawned process ids.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_listpids_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{
  // Print out the history of pids, up to the last 15 processes.
  int i = 0;
  while(i < HISTORY_SIZE && i < pid_history_index)
  {
    printf("%d: %d\n", i, pid_history[i]);
    i++;
  }

  return 1;
}

/*
* Processes when the user wants to view the most recently typed commands.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_history_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{
  int i = 0;
  int j = 0;

  // Need an extra check in case we have more than the max allowed history elements
  while(i < HISTORY_SIZE && i < cmd_history_index)
  {
    printf("%d: %s", i, command_history[i].cmd);
    for(j = 0; j < command_history[i].arg_count; j++)
    {
      printf(" %s", command_history[i].args[j]);
    }
    printf("\n");

    i++;
  }

  return 1;
}

/*
* Processes when the user enters a command that is not a default shell command. Common system directories will be searched for the executable.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_system_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{
  // Make a new process
  pid_t child_pid = fork();

  // The string that will be used to construct the different file paths
  char path_str[MAX_PATH_SIZE] = {'\0'};

  if(child_pid == -1)
  {
    // An error has occured and we are still in the parent process
  }
  else if(child_pid == 0)
  {
    // We are in the child process

    // We need to make another array of strings because execv
    // expects the filename and the array of arguments all
    // in one continguus array. Also, the array should be terminated
    // by a null pointer.
    char** corrected_args = (char**) malloc(sizeof(char*) * (arg_count + 2));

    // Copy over the command into the first argument
    corrected_args[0] = cmd;

    // Copy over the arguments
    int i;
    for(i = 0; i < arg_count; i++)
      corrected_args[i+1] = args[i];

    // Terminate the arguments
    corrected_args[arg_count + 2] = NULL;

    // Attempt to run the command in the binaries directory
    path_str[0] = '\0';
    strcat(path_str, BIN);
    strcat(path_str, cmd);
    execv(path_str, corrected_args);

    // Attempt to run the command in the current working directory
    path_str[0] = '\0';
    strcat(path_str, LOC);
    strcat(path_str, cmd);
    execv(path_str, corrected_args);

    // Attempt to run the command in the user local binary directory
    path_str[0] = '\0';
    strcat(path_str, USR_LCL_BIN);
    strcat(path_str, cmd);
    execv(path_str, corrected_args);

    // Attempt to run the command in the user binary directory
    path_str[0] = '\0';
    strcat(path_str, USR_BIN);
    strcat(path_str, cmd);
    execv(path_str, corrected_args);

    // Our shell won't support commands found in other locations
    // so it is safe to assume that the command isn't valid
    // at this point.
    printf("%s: Command not found.\n", cmd);

    // Flush now to ensure outputs aren't mixed between processes
    fflush(NULL);

    exit(0);
  }
  else
  {
    // We are in the parent process
    int status;
    int i = 0;
    waitpid(child_pid, &status, 0);

    // Put the PID in history

    // Shift the history down the array
    if(pid_history_index >= HISTORY_SIZE)
    {

      // This loop will shift all histories down a slot in the array
      while(i < HISTORY_SIZE - 1 && pid_history[i] > 0)
      {
        pid_history[i] = pid_history[i + 1];
        i++;
      }

      // Since everything has been shifted down, the last element is free to be
      // used. This implies the first element in the array becomes destroyed each time
      // a new history element is added after the max history size
      pid_history[HISTORY_SIZE - 1] = child_pid;
    }
    else
    {
      // Put the previously ran process into the history table
      pid_history[pid_history_index] = child_pid;
    }


    pid_history_index++;
  }

  return 1;
}

/*
* Splits a line delimited by whitespace.
*
* @param cmd The raw string to process.
* @param out_tokens An array of strings that will be used to store the parsed tokens.
*
* @return The amount of tokens in the specified string.
*/
int parse_line(char* cmd_str, char* out_tokens[MAX_NUM_ARGUMENTS + 1])
{
    int token_count = 0;

    // Pointer to point to the token
    // parsed by strsep
    char *arg_ptr;

    char *working_str  = strdup(cmd_str);

    // we are going to move the working_str pointer so
    // keep track of its original value so we can deallocate
    // the correct amount at the end
    char *working_root = working_str;

    // Tokenize the input stringswith whitespace used as the delimiter
    while (((arg_ptr = strsep(&working_str, WHITESPACE)) != NULL) &&
              (token_count < MAX_NUM_ARGUMENTS))
    {
      out_tokens[token_count] = strndup( arg_ptr, MAX_COMMAND_SIZE );
      if(strlen(out_tokens[token_count]) == 0 )
      {
          out_tokens[token_count] = NULL;
      }
      token_count++;
    }

/*
    int token_index  = 0;
    for( token_index = 0; token_index < token_count; token_index ++ )
    {
      printf("token[%d] = %s\n", token_index, out_tokens[token_index]);
    }*/

    free(working_root);

    return token_count;
}

/*
* Handles any command entered on the command line or previously entered via history.
*
* @param cmd The command string
* @param args An array of arguments for the function
* @param arg_count The amount of arguments
*
* @return Whether to continue execution of the shell.
*/
int handle_cmd(char* cmd, char* args[MAX_NUM_ARGUMENTS], int arg_count)
{
  int shall_continue = 1;

  if(arg_count == 0)
  {
    if(cmd[0] == '!')
    {
        int history_req = atoi(cmd + 1);

        if(history_req >= 0 && history_req < cmd_history_index && history_req < HISTORY_SIZE)
        {
          struct previous_command* prev = command_history + history_req;

          return handle_cmd(prev->cmd, (char**) prev->args, prev->arg_count);
        }
        else
        {
          printf("Command not in history.\n");
          return shall_continue;
        }
    }
  }

  // A good alternative to this would be to make a function lookup table
  // that way commands can be dynamically added without having to modify
  // this function.

  if(strcmp(cmd, QUIT_CMD) == 0 || strcmp(cmd, EXIT_CMD) == 0)
  {
    shall_continue = handle_quit_cmd(cmd, args, arg_count);
  }
  else if(strcmp(cmd, BG_CMD) == 0)
  {
    shall_continue = handle_bg_cmd(cmd, args, arg_count);
  }
  else if(strcmp(cmd, CD_CMD) == 0)
  {
    shall_continue = handle_cd_cmd(cmd, args, arg_count);
  }
  else if(strcmp(cmd, LISTPIDS_CMD) == 0)
  {
    shall_continue = handle_listpids_cmd(cmd, args, arg_count);
  }
  else if(strcmp(cmd, HISTORY_CMD) == 0)
  {
    shall_continue = handle_history_cmd(cmd, args, arg_count);
  }
  else
  {
    shall_continue = handle_system_cmd(cmd, args, arg_count);
  }

  // Put the command in the command history
  int command_index = cmd_history_index;

  // Shift command history down the array
  if(command_index >= HISTORY_SIZE)
  {
    int i = 0;
    while(i < HISTORY_SIZE - 1)
    {
      // Free arguments list
      int i;
      for(i = 0; i < command_history[i].arg_count; i++)
      {
        free(command_history[i].args[i]);
      }

      command_history[i] = command_history[i + 1];
      i++;
    }

    command_index = HISTORY_SIZE - 1;
  }

  // Copy the command over and the arguments
  strcpy(command_history[command_index].cmd, cmd);
  command_history[command_index].arg_count = arg_count;

  int i;
  for(i = 0; i < arg_count; i++)
  {
    command_history[command_index].args[i] = strdup(args[i]);
  }

  cmd_history_index++;


  return shall_continue;
}

/*
*
* The main entry point of the program.
*
* @return The status code of the shell.
*/
int main()
{

  char cmd_str[MAX_COMMAND_SIZE];
  char* tokens[MAX_NUM_ARGUMENTS];
  char* cmd = NULL;
  int shall_continue = 1;

  // Initialize pid history with zeros
  memset(pid_history, 0, HISTORY_SIZE * sizeof(pid_t));

  // Initialize handler for suspend and interrupt signals
  act_suspend.sa_sigaction = on_signal_suspend;
  act_interrupt.sa_sigaction = on_signal_interrupt;
  act_suspend.sa_flags = SA_SIGINFO;
  act_interrupt.sa_flags = SA_SIGINFO;
  sigaction(SIGTSTP, &act_suspend, NULL);
  sigaction(SIGINT, &act_interrupt, NULL);

  do
  {
    // Print out the mav shell prompt
    printf ("msh> ");

    // Read the command from the commandline.  The
    // maximum command that will be read is MAX_COMMAND_SIZE
    // This while command will wait here until the user
    // inputs something since fgets returns NULL when there
    // is no input
    while( !fgets (cmd_str, MAX_COMMAND_SIZE, stdin) );

    // Skip this line if there's no input
    if((cmd_str[0] == '\n' || cmd_str[0] == '\r'))
      continue;

    /* Split line of input into usable tokens. */
    int arg_count = parse_line(cmd_str, tokens);

    cmd = tokens[0];
    shall_continue = handle_cmd(cmd, tokens + 1, arg_count - 2);

  } while(shall_continue);

  return 0;

}
